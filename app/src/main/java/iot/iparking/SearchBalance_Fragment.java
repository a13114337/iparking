package iot.iparking;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SearchBalance_Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SearchBalance_Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchBalance_Fragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public SearchBalance_Fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SearchBalance_Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SearchBalance_Fragment newInstance(String param1, String param2) {
        SearchBalance_Fragment fragment = new SearchBalance_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_balance, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public static String m_Carid ;
    public static String m_Ownerid ;
    public static String m_Account ;
    public static String [] m_PayList ;
    public static Vector<String[]> m_Data ;
    public static boolean SearchBalance(String carid, String ownerid ) {

        m_Carid = carid ;
        m_Ownerid = ownerid ;
        try {
            DatabaseSearchBalance db = new DatabaseSearchBalance();
            m_Account  = db.execute(new String[] {  "0", carid, ownerid }).get()[0];

            if ( m_Account.length() == 0 ) return  false ;
            else return true ;

        } catch ( Exception e ) {
            Log.d( "luo", e.toString() ) ;
        }
        return false ;
    }

    public static boolean SearchPayList(String carid, String ownerid ) {

        m_Carid = carid;
        m_Ownerid = ownerid;
        try {
            DatabaseSearchBalance db = new DatabaseSearchBalance();
            // m_Account = db.execute(new String[]{carid, ownerid, "1" }).get();

            m_PayList =  db.execute(new String[] { "1", carid, ownerid }).get() ;
            if (m_PayList.length == 0) return false;
            else return true;

        } catch (Exception e) {
            Log.d("luo", e.toString());
        }
        return false;
    }

    public static boolean PayForFee( int which ) {
        try {
            DatabaseSearchBalance db = new DatabaseSearchBalance();
            // m_Account = db.execute(new String[]{carid, ownerid, "1" }).get();

            if ( db.execute(new String[] { "2", m_Carid, m_Data.get(which)[0], m_Data.get(which)[1], m_Data.get(which)[2] }).get()[0].compareTo( "true" ) == 0 )
                return true ;
            else return false ;

        } catch (Exception e) {
            Log.d("luo", e.toString());
        }

        return false ;
    }
}
class DatabaseSearchBalance extends AsyncTask<String[],Void,String[]> {
    private final String USERNAME = "root"; //定義數據庫的用戶名
    private final String PASSWORD = "root"; //定義數據庫的密碼
    private final String DRIVER = "com.mysql.jdbc.Driver";            //定義數據庫的驅動信息
    private final String URL = "jdbc:mysql://140.116.247.20:3306/iparking";    //定義訪問數據庫的地址

    public Connection mConnection;
    public  DatabaseSearchBalance() {

    }

    @Override
    protected String [] doInBackground( String[]... param ) {
        if (param[0][0].compareTo( "0" ) == 0 ) {
            String [] data =new String []{ Query(param[0][1], param[0][2]) };
            return data;
        }  // if

        else if ( param[0][0].compareTo( "1" ) == 0 ) {
            String [] data = Pay( param[0][1], param[0][2] );
            return data;
        }
        else {
            String r = ( UpdatepayState( param[0][1],param[0][2], param[0][3], Integer.parseInt(param[0][4])) ) ? "true" : "false" ;
            return  new String[]{ r } ;
        }

    }

    public String Query(String carid, String ownerid) {

        try {
            Class.forName(DRIVER).newInstance();
            mConnection = (Connection) DriverManager.getConnection(URL, USERNAME, PASSWORD);
            PreparedStatement mPrepare = mConnection.prepareStatement(
                    "select ownerid, carid, money from account " +
                            "where ( ownerid = ? and carid = ? ) ;");
            mPrepare.setNString( 1, ownerid ) ;
            mPrepare.setNString( 2, carid ) ;
            ResultSet mResultSet = mPrepare.executeQuery();
            ResultSetMetaData resultSetMateData = (ResultSetMetaData) mResultSet.getMetaData();        //獲得列的相關信息

            String data = new String() ;
            while (mResultSet.next()) {
              data += "車主身分證號：\n" ;
              data += mResultSet.getString(1) + "\n";
              data += "車牌號碼：\n" ;
              data += mResultSet.getString(2) + "\n";
              data += "餘額：\n" ;
              data += String.valueOf( mResultSet.getInt(3) ) + "\n";
            }

            return data ;
        } catch (Exception e) {

            Log.d("luo", e.toString());
        }

        return null;

    }  // Query


    public String [] Pay( String carid, String ownerid ) {

            Vector<String> datavector = new Vector<String>();
            SearchBalance_Fragment.m_Data = new Vector<String[]>() ;
            try {
                Class.forName(DRIVER).newInstance();
                mConnection = (Connection) DriverManager.getConnection(URL, USERNAME, PASSWORD);
            /* PreparedStatement mPrepare = mConnection.prepareStatement(
                    "select carid,intime,outtime,fee from parkingfee " +
                            "where intime = ( select max( intime)" +
                            " from parkingfee " +
                            " where carid = ? ) ;");
            */
                PreparedStatement mPrepare = mConnection.prepareStatement(
                        "select carid,intime,outtime,fee, pay from parkingfee " +
                                " where( carid = ? and pay = '0' ) ;");
                mPrepare.setNString( 1, carid ) ;
                ResultSet mResultSet = mPrepare.executeQuery();
                ResultSetMetaData resultSetMateData = (ResultSetMetaData) mResultSet.getMetaData();        //獲得列的相關信息
                int cols_len = resultSetMateData.getColumnCount();            //獲得列的長度




                while (mResultSet.next()) {
                    String str = new String() ;
                    String [] D = new String[3];
                    boolean out = false ;
                    for (int i = 1; i < cols_len; i++) {
                        String cols_value = mResultSet.getString(i + 1);
                        if ( i == 1 ) {
                            str += "進入時間: " ;
                            str += cols_value + "\n" ;
                            D[0] = cols_value ;

                        }  // if
                        else if ( i== 2 ) {
                            if ( cols_value.compareTo( "000000000000" ) == 0 ) ;
                            else {
                                out = true ;
                                str += "出場時間: " ;
                                str += cols_value + "\n" ;
                                D[1] = cols_value ;
                            }  // else
                        }  // else if
                        else if ( i== 3 ) {
                            if ( !out ) ;
                            else {
                                str += "停車費: ";
                                str += cols_value + "\n" ;
                                D[2] = cols_value ;
                            }  // else
                        }  // else if
                    }
                    if ( out ) {
                        datavector.add(str);
                        SearchBalance_Fragment.m_Data.add(D);
                    } // if
                }

                String[] data = new String [ datavector.size() ] ;
                datavector.toArray( data );
                return data ;
        } catch( Exception e ) {
            Log.d("luo", e.toString());
        }
        return null ;
    }

    public boolean UpdatepayState( String carid, String intime, String outtime, int fee ) {
        try {
            Class.forName(DRIVER).newInstance();
            mConnection = (Connection) DriverManager.getConnection(URL, USERNAME, PASSWORD);
            /* PreparedStatement mPrepare = mConnection.prepareStatement(
                    "select carid,intime,outtime,fee from parkingfee " +
                            "where intime = ( select max( intime)" +
                            " from parkingfee " +
                            " where carid = ? ) ;");
            */
            PreparedStatement mPrepare ;

            mPrepare = mConnection.prepareStatement(
                    "select money from account " +
                            "where ( ownerid = ? and carid = ? ) ;");
            mPrepare.setNString( 1, SearchBalance_Fragment.m_Ownerid ) ;
            mPrepare.setNString( 2, carid ) ;
            ResultSet mResultSet = mPrepare.executeQuery();

            int money = 0 ;
            if ( mResultSet.next() ) money = mResultSet.getInt(1) ;
            else return false ;


            money -= fee ;
            if ( money < 0 ) return false ;

             mPrepare = mConnection.prepareStatement(
                    "UPDATE parkingfee SET pay = 1 " +
                            "WHERE ( carid = ? " +
                            "AND intime = ? " +
                            "AND outtime = ? );");
            mPrepare.setNString(1, carid);
            mPrepare.setNString(2, intime);
            mPrepare.setNString(3, outtime);

            PreparedStatement mPrepare2 = mConnection.prepareStatement(
            "UPDATE account  SET  money = ? " +
            "where ( ownerid = ? and carid = ? ) ;") ;

            mPrepare2.setInt(1, money);
            mPrepare2.setNString(2, SearchBalance_Fragment.m_Ownerid);
            mPrepare2.setNString(3, carid);

            //Log.d("luo", carid + " " + intime + " " + outtime  );
            return (  mPrepare.executeUpdate() == 1 & mPrepare2.executeUpdate()==1 ) ? true : false ;
        } catch( Exception e ) {
            Log.d("luo", e.toString());
        }
        return false ;
    }
}