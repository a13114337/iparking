package iot.iparking;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ParkingState_Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ParkingState_Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ParkingState_Fragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ParkingState_Fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ParkingState_Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ParkingState_Fragment newInstance(String param1, String param2) {
        ParkingState_Fragment fragment = new ParkingState_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_parking_state, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
class DatabaseSearchState extends AsyncTask<Void,Void,String[]> {
    private final String USERNAME = "root"; //定義數據庫的用戶名
    private final String PASSWORD = "root"; //定義數據庫的密碼
    private final String DRIVER = "com.mysql.jdbc.Driver";            //定義數據庫的驅動信息
    private final String URL = "jdbc:mysql://140.116.247.20:3306/iparking";    //定義訪問數據庫的地址

    public Connection mConnection;
    public DatabaseSearchState() {

    }

    @Override
    protected String[] doInBackground( Void... param ) {
        String [] data = (String[]) Query();
        return data ;
    }

    public Object[] Query() {
        Vector<String> datavector = new Vector<String>();

        try {
            Class.forName(DRIVER).newInstance();
            mConnection = (Connection) DriverManager.getConnection(URL, USERNAME, PASSWORD);
            PreparedStatement mPrepare = mConnection.prepareStatement("select state from parkingstate;");
            ResultSet mResultSet = mPrepare.executeQuery();
            while (mResultSet.next()) {
                String str = mResultSet.getString(1);
                datavector.add(str);
            }

            String[] data = new String [ datavector.size() ] ;
            datavector.toArray( data );
            return data ;
        } catch (Exception e) {
            Log.d("luo", e.toString());
        }

        return null;

    }  // Query
}