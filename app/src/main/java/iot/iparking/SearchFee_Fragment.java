package iot.iparking;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;



/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SearchFee_Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SearchFee_Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchFee_Fragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    public static String [] carState ;
    public static String m_Carid ;
    public static boolean searchdone ;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private EditText mCarId ;
    public SearchFee_Fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SearchFee_Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SearchFee_Fragment newInstance(String param1, String param2) {
        SearchFee_Fragment fragment = new SearchFee_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        m_Carid = null ;
        carState = null ;

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_fee, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    public static boolean SearchCarFee(String carid) {

        m_Carid = carid ;
        try {
            DatabaseSearchFee db = new DatabaseSearchFee();
            carState = db.execute(carid).get();

            if ( carState.length == 0 ) return  false ;
            else return true ;

        } catch ( Exception e ) {
            Log.d( "luo", e.toString() ) ;
        }
        return false ;
    }
}
class DatabaseSearchFee extends AsyncTask<String,Void,String[]> {
    private final String USERNAME = "root"; //定義數據庫的用戶名
    private final String PASSWORD = "root"; //定義數據庫的密碼
    private final String DRIVER = "com.mysql.jdbc.Driver";            //定義數據庫的驅動信息
    private final String URL = "jdbc:mysql://140.116.247.20:3306/iparking";    //定義訪問數據庫的地址

    public Connection mConnection;
    public DatabaseSearchFee() {

    }

    @Override
    protected String[] doInBackground( String... param ) {
        String [] data = (String[]) Query( param[0] );
        return data ;
    }

    public Object[] Query(String carid) {
        Vector<String> datavector = new Vector<String>();

        try {
            Class.forName(DRIVER).newInstance();
            mConnection = (Connection) DriverManager.getConnection(URL, USERNAME, PASSWORD);
            /* PreparedStatement mPrepare = mConnection.prepareStatement(
                    "select carid,intime,outtime,fee from parkingfee " +
                            "where intime = ( select max( intime)" +
                            " from parkingfee " +
                            " where carid = ? ) ;");
            */
            PreparedStatement mPrepare = mConnection.prepareStatement(
                    "select carid,intime,outtime,fee from parkingfee " +
                            " where( carid = ? ) ;");
            mPrepare.setNString( 1, carid ) ;
            ResultSet mResultSet = mPrepare.executeQuery();
            ResultSetMetaData resultSetMateData = (ResultSetMetaData) mResultSet.getMetaData();        //獲得列的相關信息
            int cols_len = resultSetMateData.getColumnCount();            //獲得列的長度

            String intime = new String() ;
            String nowtime = new String() ;
            while (mResultSet.next()) {
                boolean out = false ;
                String str = new String() ;
                for (int i = 1; i < cols_len; i++) {
                    String cols_value = mResultSet.getString(i + 1);
                    if ( i == 1 ) {
                        str += "進入時間: " ;
                        str += cols_value + "\n" ;
                        intime = cols_value ;
                    }  // if
                    else if ( i== 2 ) {

                        if ( cols_value.compareTo( "000000000000" ) == 0 ) {
                            str += "目前時間: " ;
                            Calendar c = Calendar.getInstance();
                            SimpleDateFormat simple = new SimpleDateFormat();
                            simple.applyPattern("yyyyMMddHHmm");
                            nowtime +=  simple.format(c.getTime());
                            str += nowtime + "\n" ;
                        }  // if

                        else {
                            out = true ;
                            str += "出場時間: " ;
                            str += cols_value + "\n" ;
                        }  // else
                    }  // else if
                    else if ( i== 3 ) {
                        if ( !out ) {
                            str += "停車費試算: ";
                            SimpleDateFormat simple = new SimpleDateFormat();
                            simple.applyPattern("yyyyMMddHHmm");
                            Date time1 = simple.parse( intime ) ;
                            Date time2 = simple.parse( nowtime ) ;
                            long diff = time2.getTime() - time1.getTime() ;
                            long count = diff / (1000*60*60) ;
                            if ( diff % (1000*60*60) > 100  ) count++ ;
                            Log.d( "luo", String.valueOf( diff )  ) ;
                            str += ( count * 30 ) ;
                        } // if
                        else {
                            str += "停車費: ";
                            str += cols_value + "\n" ;
                        }  // else
                    }  // else if
                }
                // Log.d( "luo", str ) ;
                datavector.add(str);
            }

            String[] data = new String [ datavector.size() ] ;
            datavector.toArray( data );
            return data ;
        } catch (Exception e) {

            Log.d("luo",  e.toString());
        }

        return null;

    }  // Query
}