package iot.iparking;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_main);
        jumpToMainLayout();
    }



    public void jumpToSearchFeeLayout(){

        setContentView(R.layout.fragment_search_fee);

        Button ReturnMainPage= (Button)findViewById(R.id.ReturnMainLayout);
        final EditText carId = (EditText) findViewById(R.id.carId) ;
        Button SearchButton = (Button) findViewById( R.id.SesrchFeeButtton ) ;
        assert SearchButton != null;
        SearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (carId.getText().toString().compareTo("範例:245-ABC") != 0) {
                        boolean findCar = SearchFee_Fragment.SearchCarFee(carId.getText().toString().toUpperCase());

                        if ( findCar ) {
                            new AlertDialog.Builder(MainActivity.this).setTitle("車號:" + SearchFee_Fragment.m_Carid).setItems(
                                    SearchFee_Fragment.carState, null).setNegativeButton("OK", null).show();


                        } else
                            new AlertDialog.Builder(MainActivity.this).setTitle("錯誤").setItems(
                                    new String[]{"查無此車牌號碼"}, null).setNegativeButton("OK", null).show();
                    } // if
                    else {
                        new AlertDialog.Builder(MainActivity.this).setTitle("提醒").setItems(
                                new String[]{"請輸入車牌號碼"}, null).setNegativeButton("确定", null).show();

                    }  // else
                } catch( Exception e  ) {
                   Log.d( "luo", e.toString() ) ;
                }
            }
        }); ;
        assert carId != null;
        carId.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus == true) {
                    carId.setText("");
                }
            }
        });

        ReturnMainPage.setOnClickListener(new Button.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                jumpToMainLayout();
            }

        });

    }

    public void jumpToMainLayout(){

        setContentView(R.layout.activity_main);
        Button SearchFeeButton = (Button)findViewById(R.id.SearchFee);
        SearchFeeButton.setOnClickListener(new Button.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                jumpToSearchFeeLayout();
            }

        });

        Button ParkingstateButton = (Button)findViewById(R.id.ParkingState);
        ParkingstateButton.setOnClickListener(new Button.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                jumpToParkingStateLayout();
            }

        });

        Button BalanceButton = (Button)findViewById(R.id.Balance);
        BalanceButton.setOnClickListener(new Button.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                jumpToSearchBalance() ;
            }

        });


    }

    public void jumpToParkingStateLayout(){

        setContentView(R.layout.fragment_parking_state);

        Button ReturnMainPage= (Button)findViewById(R.id.ReturnMainLayout);

        ReturnMainPage.setOnClickListener(new Button.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                jumpToMainLayout();
            }

        });

        DatabaseSearchState db = new DatabaseSearchState() ;
        try {
            String [] state = db.execute().get();
            Resources res = getResources(); //if you are in an activity
            for ( int i = 0 ; i < state.length ; i++ ) {
                if ( state[i].compareTo( "0" ) == 0 ) {
                    String idName = "num" + ( i + 1 ) ;
                    ImageView iv = (ImageView) findViewById(res.getIdentifier(idName, "id", getPackageName())) ;
                    iv.setBackgroundResource(R.drawable.green);
                }  // if
                else if ( state[i].compareTo( "1" ) == 0 ){
                    String idName = "num" + ( i + 1 ) ;
                    ImageView iv = (ImageView) findViewById(res.getIdentifier(idName, "id", getPackageName())) ;
                    iv.setBackgroundResource( R.drawable.red);
                }  // else if
            }  // for
        } catch ( Exception e ) {
            Log.d( "luo", e.toString() ) ;
        }

    }

    public void jumpToSearchBalance() {
        setContentView(R.layout.fragment_search_balance);

        Button ReturnMainPage= (Button)findViewById(R.id.ReturnMainLayout);

        ReturnMainPage.setOnClickListener(new Button.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                jumpToMainLayout();
            }

        });

        final EditText carId = (EditText) findViewById(R.id.carid) ;

        carId.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus == true) {
                    carId.setText("");
                }
            }
        });

        final EditText onwerid = (EditText) findViewById(R.id.ownerid) ;

        onwerid.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus == true) {
                    onwerid.setText("");
                }
            }
        });


        final Button SearchButton = (Button) findViewById( R.id.SearchBalance ) ;
        assert SearchButton != null;
        SearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (carId.getText().toString().compareTo("範例:245-ABC") != 0 && onwerid.getText().toString().compareTo("範例:C123456789") != 0) {
                        boolean findCar = SearchBalance_Fragment.SearchBalance(carId.getText().toString().toUpperCase(), onwerid.getText().toString().toUpperCase());

                        if (findCar)
                            new AlertDialog.Builder(MainActivity.this).setTitle("查詢餘額:").setItems(
                                    new String[]{SearchBalance_Fragment.m_Account}, null).setNegativeButton("OK", null).show();
                        else
                            new AlertDialog.Builder(MainActivity.this).setTitle("錯誤").setItems(
                                    new String[]{"查無資料"}, null).setNegativeButton("OK", null).show();
                    } // if
                    else {
                        new AlertDialog.Builder(MainActivity.this).setTitle("提醒").setItems(
                                new String[]{"請輸入正確的資訊"}, null).setNegativeButton("确定", null).show();

                    }  // else
                } catch (Exception e) {
                    Log.d("luo", e.toString());
                }
            }
        });


        Button PayButton = (Button) findViewById( R.id.pay ) ;
        assert PayButton != null;
        PayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (carId.getText().toString().compareTo("範例:245-ABC") != 0 && onwerid.getText().toString().compareTo("範例:C123456789") != 0) {
                        boolean findCar = SearchBalance_Fragment.SearchPayList(carId.getText().toString().toUpperCase(), onwerid.getText().toString().toUpperCase());

                        if (findCar) {

                            final String[] result = SearchBalance_Fragment.m_PayList ;

                            AlertDialog.Builder dialog_list = new AlertDialog.Builder(MainActivity.this);
                            dialog_list.setTitle("請選擇項目繳費");
                            dialog_list.setItems(result, new DialogInterface.OnClickListener(){
                                @Override

                                //只要你在onClick處理事件內，使用which參數，就可以知道按下陣列裡的哪一個了
                                public void onClick(DialogInterface dialog, int which) {
                                    // TODO Auto-generated method stub
                                    if ( SearchBalance_Fragment.PayForFee( which ) )
                                      Toast.makeText(MainActivity.this, "此筆項目 已繳費：\n" + result[which], Toast.LENGTH_SHORT).show();
                                    else
                                      Toast.makeText(MainActivity.this, "繳費失敗或餘額不足 請重試", Toast.LENGTH_SHORT).show();
                                }
                            });
                            dialog_list.show();
                        }  // if
                        else
                            new AlertDialog.Builder(MainActivity.this).setTitle("繳費資訊").setItems(
                                    new String[]{"查無資料 或 輸入資料錯誤"}, null).setNegativeButton("OK", null).show();
                    } // if
                    else {
                        new AlertDialog.Builder(MainActivity.this).setTitle("提醒").setItems(
                                new String[]{"請輸入正確的資訊"}, null).setNegativeButton("确定", null).show();

                    }  // else
                } catch (Exception e) {
                    Log.d("luo", e.toString());
                }


            }
        });

    }

}
